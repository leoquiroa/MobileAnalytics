/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LDN.cpp
 * Author: mario
 * 
 * Created on August 29, 2018, 6:36 PM
 */

#include "LTP.h"

/*
* src - input
* dst - output
* ddepth - depth destination image
* kernel - convolution mask
* anchor - default (-1,-1) means center
* delta - extra value added before store
* borderType - enum BorderTypes in base.hpp
* 
* filter2D
 * (
 *          InputArray src, 
 *          OutputArray dst, 
 *          int ddepth, 
 *          InputArray kernel, 
 *          Point anchor=Point(-1,-1), 
 *          double delta=0, 
 *          int borderType=BORDER_DEFAULT 
 * )
*/

/*
 * src - input
 * dst - output
 * thresh -threshold value
 * maxval - value to replace
 * type - enum ThresholdTypes in imgproc.hpp
 * 
 * threshold
 * ( 
 *          InputArray src, 
 *          OutputArray dst,
 *          double thresh, 
 *          double maxval, 
 *          int type 
 * )
 */


cv::Mat LTP::calculateLTP(string folderGray, int NoFrame) 
{
    //build the image path
    string imageName = folderGray+ "/f" + std::to_string(NoFrame) + ".png";
    //read the image
    cv::Mat img = cv::imread(imageName, CV_LOAD_IMAGE_GRAYSCALE);
    //get dimensions
    int h = img.rows;
    int w = img.cols;
    //total maks
    int depth = 8;
    //threshold
    int thdLTP = 5;
    //index of the mask
    int coord[depth][2] = {{1,2},{0,2},{0,1},{0,0},{1,0},{2,0},{2,1},{2,2}};
    //results positive and negative codified
    Mat codePosMat = Mat::zeros(h, w, CV_8U );
    Mat codeNegMat = Mat::zeros(h, w, CV_8U );
    //iterate over the eight masks
    for (int ixCrop = 0; ixCrop < depth; ++ixCrop)
    {
        //create the mask 3x3
        cv::Mat Mask = Mat::zeros(3, 3, CV_8S );
        Mask.at<uchar>(1,1) = -1;
        Mask.at<uchar>(coord[ixCrop][0],coord[ixCrop][1]) = 1;
        //filter the image
        cv::Mat imgFilter = Mat(h, w, CV_16S );
        cv::filter2D(img, imgFilter, CV_16S , Mask, Point( -1, -1 ), 0, BORDER_DEFAULT );
        //save the filterd image
        //cv::imwrite("video1Filter/f" + std::to_string(ixCrop) + ".png", imgFilter);
        //results of the image above, below and in the middle
        Mat thdMat = Mat::zeros(h, w, CV_8U );
        Mat posMat = Mat::zeros(h, w, CV_8U );
        Mat negMat = Mat::zeros(h, w, CV_8U );
        //thresholding the image, discarding all the values less than in abs val
        cv::threshold(abs(imgFilter) , thdMat, thdLTP, 1, THRESH_BINARY);
        //keep only positive values
        cv::threshold(imgFilter, posMat, 0, 1, THRESH_BINARY);
        posMat = thdMat.mul(posMat);
        //keep only negative values
        cv::threshold(imgFilter, negMat, 0, 1, THRESH_BINARY_INV);
        negMat = thdMat.mul(negMat);
        //binary codification
        cv::add(posMat*std::pow(2,ixCrop), codePosMat, codePosMat, Mat(), CV_8U);
        cv::add(negMat*std::pow(2,ixCrop), codeNegMat, codeNegMat, Mat(), CV_8U);
        //namedWindow( "Display window" + ixCrop, WINDOW_AUTOSIZE );
        //imshow( "Display window", imgFilter );                   
        //waitKey(0);
    }
    Mat concatMat = Mat::zeros(h*2, w, CV_8U );
    cv::vconcat(codePosMat, codeNegMat, concatMat);
    //namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    //imshow( "Display window", img );                   // Show our image inside it.
    //waitKey(0);                                          // Wait for a keystroke in the window
    //cout << folderGray << "/f" << NoFrame << ".png" << endl;
    return concatMat;
}



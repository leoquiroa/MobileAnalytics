/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LDN.h
 * Author: mario
 *
 * Created on August 29, 2018, 6:36 PM
 */

#ifndef LTP_H
#define LTP_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

class LTP
{
    public:
        cv::Mat calculateLTP(string folderGray,int NoFrame);
    private:

};

#endif /* LTP_H */


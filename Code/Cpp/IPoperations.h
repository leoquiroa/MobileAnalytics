
#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <vector>

using namespace std;
using namespace cv;

class morph {
	public:
		int diamond;
		int rectangle;
		morph(int _d, int _r) {
			diamond = _d;
			rectangle = _r;
		}
		morph() {}
};

class IPoperations
{
	
	string fileNamePerLabel = "cropList.0418.csv";
	string cropFolder = "crop\\";
	string originalFolder = "original\\";
	int initialNumber = 100;
	///canny parameters
	int lowThd = 50;                                                                                    ///param
	int ratio = 3;                                                                                      ///param
	int kernel_size = 3;
	///parameters for the filter
	static const int depth = 8;
	int matDepth = 3;
	Point anchor = Point(-1, -1);
	double delta = 0;

	public:
		
		cv::Mat getGrayImage(string imageName);
		cv::Mat getCannyImage(cv::Mat imgGrey);
		cv::Mat applyMorphologyOps(cv::Mat detected_edges, int morph_size_close, int morph_size_erode);
		void classifyCrops(
			Mat labels, Mat stats, int ixStruct, Mat imgGrey, vector<morph> structure, string extension, 
			int ixImg, vector<string> &finalTable, float percToKeep, float minThdArea, float maxThdArea);
		
		void writeFile(vector<string> finalTable);
		void cleanFile();

		cv::Mat Descriptor(std::string cropName, int NoDivHist, int thdLTP);
		cv::Mat getHistogram(int w, int h, int NoDivHist, cv::Mat concatMat);
};
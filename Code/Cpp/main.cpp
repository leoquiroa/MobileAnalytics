/* 
 * File:   main.cpp
 * Author: mario
 *
 * Created on August 28, 2018, 2:07 PM
 */

#include "Split.h"
#include "Classifier.h"
#include "Descriptor.h"

int main(int argc, char** argv) 
{
    //Split *s = new Split("video1.mp4");
    //Classifier *c = new Classifier(s->NoFrames, s->baseFolder, s->folderName);
    
    //Classifier *c = new Classifier(655, "video1", "video1Gray");
    Descriptor *d = new Descriptor("video1", "video1Gray");
    
    //delete s;
    //delete c;
    delete d;

    return 0;
}



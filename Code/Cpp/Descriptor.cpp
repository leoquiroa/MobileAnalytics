/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Descriptor.cpp
 * Author: mario
 * 
 * Created on August 29, 2018, 6:35 PM
 */

#include "Descriptor.h"
#include "LTP.h"
#include "LDN.h"
#include "Histogram.h"

Descriptor::Descriptor(string baseFolder, string folderGray)
{
    std::ifstream infile("Table." + baseFolder + "." + tool.currentDate() + ".txt");
    string line;
    
    LDN ldn;
    LTP ltp;
    Histogram hist;
    
    Mat ListDescriptors, ListLabels;
    int tempFlag = 0;
    while (std::getline(infile, line))
    {
        std::istringstream iss(line);
        int frameNo;
        char comma, frameType;
        if (!(iss >> frameNo >> comma >> frameType)) 
        { 
            break;  // error
        }
        if(frameType == 'S')
        {
            /*
            cv::Mat signSquare = ltp.calculateLTP(folderGray,frameNo);
            cv::Mat signLinear = hist.calculateHistogram(signSquare);
            
            ListDescriptors.push_back(signLinear.t());
            ListLabels.push_back(frameNo);
            */
            
            cv::Mat signSquare = ldn.calculateLDN(folderGray,frameNo);

            tempFlag++;
            if(tempFlag == 1)
                break;
        }
    }
    infile.close();
    writeFileDescriptor(ListDescriptors, baseFolder);
    writeFileLabel(ListLabels, baseFolder);
}

void Descriptor::writeFileDescriptor(cv::Mat ListDescriptors, string baseFolder)
{
    ofstream myfile ("Desc." + baseFolder + "." + tool.currentDate() + ".txt");
    for(int y = 0; y < ListDescriptors.rows; y++)
    {
        for (int x = 0; x < ListDescriptors.cols; ++x)
        {
            myfile << to_string(ListDescriptors.at<float>(y,x)) + ",";
        }
        myfile << "\n";        
    }
    myfile.close();
}

void Descriptor::writeFileLabel(cv::Mat ListLabels, string baseFolder)
{
    ofstream myfile ("Labl." + baseFolder + "." + tool.currentDate() + ".txt");
    for(int f = 0; f < ListLabels.rows; f++)
    {
        myfile << to_string(ListLabels.at<int>(f,0)) + "\n";
    }
    myfile.close();
}


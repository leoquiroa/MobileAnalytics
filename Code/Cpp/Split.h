/* 
 * File:   Split.h
 * Author: mario
 *
 * Created on August 28, 2018, 3:43 PM
 */

#ifndef SPLIT_H
#define SPLIT_H

#include <opencv2/opencv.hpp>
#include "Tools.h"

using namespace std;
using namespace cv;

class Split {
    Tools tool;
    public:
        Split();
        Split(const char* fileName);
        cv::Mat sliceMat(cv::Mat L,int dim,std::vector<int> _sz);
        ofstream LogFile;
        
        cv::Mat frame;
        cv::Mat bwFrame;
        
        string folderName;
        int NoFrames = 0;
	string baseFolder;
    private:
	bool ValidVideo(cv::VideoCapture capt);
	bool InvalidFrame(cv::Mat fram);
	cv::Mat ToGray(cv::Mat fram);
	bool saveFrame(cv::Mat fram, int NoFram);
};

#endif /* SPLIT_H */


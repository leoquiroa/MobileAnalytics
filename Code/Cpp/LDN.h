/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LDN.h
 * Author: mario
 *
 * Created on September 13, 2018, 1:11 PM
 */

#ifndef LDN_H
#define LDN_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

class LDN {
        int xx = 0;
        int yy = 0;
    public:
        cv::Mat calculateLDN(string folderGray,int NoFrame);
        void dump3DFloat(cv::Mat m);
        void dump3DInt(cv::Mat m);
        void dump2DInt(cv::Mat m);
        void dump2DFloat(cv::Mat m);
        
        cv::Mat ProminentDirection(cv::Mat MatIndex, int direction);
        cv::Mat sortInDepth(cv::Mat MatValues);
        void fillCube(Mat &MatResize2D);
    private:

};

#endif /* LDN_H */


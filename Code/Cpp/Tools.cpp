/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Tools.cpp
 * Author: mario
 * 
 * Created on August 29, 2018, 10:51 AM
 */

#include "Tools.h"

string Tools::getFileName(const char* fileName) 
{
    char * fileNameCast = strdup(fileName);
    char * pch = strtok(fileNameCast, ".");
    return pch;
}
string Tools::createFolder(string fileName, string postFix) 
{
    string folderName = fileName + postFix;
    mkdir(folderName.c_str(),0777);
    return folderName;
}
string Tools::currentDate() 
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y.%m.%d", &tstruct);
    return buf;
}
string Tools::currentTime() 
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);
    return buf;
}

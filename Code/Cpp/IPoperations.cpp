#include "IPoperations.h"



cv::Mat IPoperations::getGrayImage(string imageName)
{
	///load image
	Mat imgColor;
	try
	{
		imgColor = imread(originalFolder + imageName, CV_LOAD_IMAGE_COLOR);
	}
	catch (const std::exception&)
	{
		cout << "Cannot load the image" << endl;
		return Mat();
	}
	
	if (imgColor.rows == 0)
		return Mat();
	Mat imgGrey;
	cv::cvtColor(imgColor, imgGrey, COLOR_BGR2GRAY);
	return imgGrey;
}
cv::Mat IPoperations::getCannyImage(cv::Mat imgGrey)
{
	///reduce noise with a 3x3 kernel
	Mat detected_edges;
	cv::blur(imgGrey, detected_edges, Size(3, 3));
	///canny edge detector                                                                             ///param
	cv::Canny(detected_edges, detected_edges, lowThd, lowThd*ratio, kernel_size);
	return detected_edges;
}
cv::Mat IPoperations::applyMorphologyOps(cv::Mat detected_edges, int morph_size_close, int morph_size_erode)
{
	///close
	Mat imgClose;
	Mat element_close = getStructuringElement(
		MORPH_ELLIPSE,
		Size(morph_size_close, morph_size_close));       ///param
	cv::morphologyEx(detected_edges, imgClose, MORPH_CLOSE, element_close);
	///erode
	Mat imgErode;
	Mat element_erode = getStructuringElement(
		MORPH_RECT,
		Size(morph_size_erode, 3));        ///param
	cv::morphologyEx(imgClose, imgErode, MORPH_ERODE, element_erode);
	return imgErode;
}
void IPoperations::classifyCrops(
	Mat labels, Mat stats, int ixStruct, Mat imgGrey, vector<morph> structure, string extension, 
	int ixImg, vector<string>& finalTable, float percToKeep, float minThdArea, float maxThdArea)
{
	///limits
	int pass = 0;
	float maxHeight = (float)(labels.rows * percToKeep);                                                            ///param
	float maxWidth = (float)(labels.cols * percToKeep);                                                             ///param
	int totalArea = labels.cols * labels.rows;
	///iterator over all the possible images
	cout << "possible crops " << stats.rows << ", iteration " << ixStruct << endl;
	for (int ixCrop = 0; ixCrop < stats.rows; ++ixCrop) {
		///extract information from stats
		int x = stats.at<int>(Point(0, ixCrop));
		int y = stats.at<int>(Point(1, ixCrop));
		int w = stats.at<int>(Point(2, ixCrop));
		int h = stats.at<int>(Point(3, ixCrop));
		int a = stats.at<int>(Point(4, ixCrop));
		float p = ((float)a / (float)totalArea) * 100;
		///big condition
		if (p > minThdArea && p < maxThdArea &&
			w > maxThdArea && w < maxWidth &&
			h > maxThdArea && h < maxHeight)
		{
			pass++;
			//cout << "crop[" << ixCrop  << "] " << endl;
			///crop
			Mat ROI(imgGrey, Rect(x, y, w, h));
			///copy
			Mat crop;
			ROI.copyTo(crop);
			///create name and write file
			string cropName =
				to_string(initialNumber + ixImg) +
				to_string(structure[ixStruct].diamond) +
				to_string(structure[ixStruct].rectangle) +
				to_string(initialNumber + ixCrop) + "." + extension;
			///store in the vector
			finalTable.push_back(string(cropName));
			try
			{
				imwrite(cropFolder + cropName, crop);
			}
			catch (const std::exception&)
			{
				cout << "cannot save crop images" << endl;
			}
			
		}///end if
	}///end for stats
	cout << "crops to analyze " << pass << endl;		
}

void IPoperations::writeFile(vector<string> finalTable) {
	ofstream myfile;
	myfile.open(fileNamePerLabel, std::fstream::app);
	for (size_t i = 0; i < finalTable.size(); i++)
	{
		myfile << finalTable[i] << "," << to_string(0) << "\n";
	}
	myfile.close();
}

void IPoperations::cleanFile() {
	ofstream myfile;
	myfile.open(fileNamePerLabel, std::fstream::trunc);
	myfile.close();
}

cv::Mat IPoperations::Descriptor(std::string cropName, int NoDivHist, int thdLTP)
{
	///read image
	Mat imgCrop;
	try
	{
		imgCrop = imread(cropFolder + cropName, CV_LOAD_IMAGE_GRAYSCALE);                                   ///param
	}
	catch (const std::exception&)
	{
		cout << "Cannot load the crop-image" << endl;
		return Mat();
	}
	
	int h = imgCrop.rows;
	int w = imgCrop.cols;
	///cordinates for the masks
	uchar coord[depth][2] = { { 1,2 },{ 0,2 },{ 0,1 },{ 0,0 },{ 1,0 },{ 2,0 },{ 2,1 },{ 2,2 } };
	///result
	Mat codePosMat = Mat::zeros(h, w, CV_8U);
	Mat codeNegMat = Mat::zeros(h, w, CV_8U);
	for (int ixCrop = 0; ixCrop < depth; ++ixCrop)
	{
		///mask
		Mat Mask = Mat::zeros(3, 3, CV_8S);
		Mask.at<uchar>(1, 1) = -1;
		Mask.at<uchar>(coord[ixCrop][0], coord[ixCrop][1]) = 1;
		///filter
		Mat imgFilter = Mat(h, w, CV_16S);
		cv::filter2D(imgCrop, imgFilter, matDepth, Mask, anchor, delta, BORDER_DEFAULT);
		///vectors of mat for the threshold
		Mat thdMat = Mat::zeros(h, w, CV_8U);
		Mat posMat = Mat::zeros(h, w, CV_8U);
		Mat negMat = Mat::zeros(h, w, CV_8U);
		///discard values less than in absolute value
		cv::threshold(abs(imgFilter), thdMat, thdLTP, 1, 0);
		///keep only positive responses, discarding others
		cv::threshold(imgFilter, posMat, 0, 1, 0);
		posMat = thdMat.mul(posMat);
		///keep only negative responses
		cv::threshold(imgFilter, negMat, 0, 1, 1);
		negMat = thdMat.mul(negMat);
		///binary codification
		cv::add(posMat*std::pow(2, ixCrop), codePosMat, codePosMat, Mat(), CV_8U);
		cv::add(negMat*std::pow(2, ixCrop), codeNegMat, codeNegMat, Mat(), CV_8U);
	}
	///concatenate image
	Mat concatMat = Mat::zeros(h * 2, w, CV_8U);
	cv::vconcat(codePosMat, codeNegMat, concatMat);
	///concatenate histogram
	return getHistogram(w, h, NoDivHist, concatMat);
}
cv::Mat IPoperations::getHistogram(int w, int h, int NoDivHist, cv::Mat concatMat)
{
	///divide images
	int step = (h * 2) / NoDivHist;
	///Establish the number of bins
	int histSize = 256;
	///Set the ranges ( for B,G,R) )
	float range[] = { 0, 255 };
	const float* histRange = { range };
	///
	bool uniform = true;
	bool accumulate = false;
	///
	Mat concatHist, normHist;
	for (int ixHist = 0; ixHist < NoDivHist; ++ixHist)
	{
		/// ROI
		Mat ROIpiece(concatMat, Rect(0, ixHist*step, w, step));
		///copy
		Mat piece; ROIpiece.copyTo(piece);
		///histogram
		Mat histMat;
		cv::calcHist(&piece, 1, 0, Mat(), histMat, 1, &histSize, &histRange, uniform, accumulate);
		///concatenation
		concatHist.push_back(histMat);
	}
	//normalize to [1-1000]
	cv::normalize(concatHist, normHist, 1, 1000, NORM_MINMAX, -1, Mat());
	return normHist;
}
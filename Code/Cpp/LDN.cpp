/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LDN.cpp
 * Author: mario
 * 
 * Created on September 13, 2018, 1:11 PM
 */

#include "LDN.h"

cv::Mat LDN::calculateLDN(string folderGray, int NoFrame) 
{
    
    
    
        
    int x = 5;
    int y = 3;
    int z = 4;
    
    xx = 5;
    yy = 3;
    
    //original
    std::vector<int> cube_size{x,y,z};
    cv::Mat MatCube(3, cube_size.data(), CV_32FC1, cv::Scalar(0.0));
    
    //sort
    std::vector<int> original_size(MatCube.size.p, MatCube.size.p + 3);
    std::vector<int> reshape_size{ original_size[0] * original_size[1], original_size[2] };
    cv::Mat MatResize2D(MatCube.reshape(1, reshape_size));
    //cout << "resize" << endl;
    //dump2DFloat(MatResize2D);
    
    fillCube(MatResize2D);
    
    
    
    
    
    //get matrix of index
    cv::Mat MatIndex = sortInDepth(MatResize2D);
    //get 1st prominent
    cv::Mat matTop1 = ProminentDirection(MatIndex, 0);
    //get 2nd prominent
    cv::Mat matTop2 = ProminentDirection(MatIndex, 1);
    
    cout << "insert new Image" << endl;
    dump2DFloat(MatResize2D);
    cout << "indices" << endl;
    dump2DInt(MatIndex);
    cout << "prominents" << endl;
    dump2DInt(matTop1);
    dump2DInt(matTop2);
        
    cv::Mat concatMat;
    return concatMat;
}

void LDN::fillCube(Mat &MatResize2D)
{
    cv::Mat C = (Mat_<float>(5,3) << 11.0,0.0,0.0,    1.0,1.0,1.0,      2.0,2.0,2.0,      3.0,3.0,3.0,      4.0,4.0,4.0);
    cv::Mat D = (Mat_<float>(5,3) << 19.0,5.0,5.0,    66.0,6.0,6.0,      7.0,7.0,7.0,      8.0,88.0,8.0,      9.0,9.0,9);
    cv::Mat E = (Mat_<float>(5,3) << 12.0,10.0,10.0, 11.0,11.0,11.0,   12.0,12.0,12.0,   13.0,13.0,13.0,   14.0,14.0,14);
    cv::Mat F = (Mat_<float>(5,3) << 15.0,15.0,15.0, 16.0,16.0,16.0,   17.0,17.0,17.0,   18.0,18.0,18.0,   19.0,19.0,19);
    
    //cout << "new Image" << endl;
    //dump2DFloat(C);
    
    Mat c = C.reshape(1,15);
    Mat d = D.reshape(1,15);
    Mat e = E.reshape(1,15);
    Mat f = F.reshape(1,15);
    
    //cout << "reshape new Image" << endl;
    //dump2DFloat(c);
    //dump2DFloat(d);
    //dump2DFloat(e);
    //dump2DFloat(f);
    
    
    c.copyTo(MatResize2D.col(0));
    d.copyTo(MatResize2D.col(1));
    e.copyTo(MatResize2D.col(2));
    f.copyTo(MatResize2D.col(3));
}

cv::Mat LDN::sortInDepth(cv::Mat MatValues)
{
    cv::Mat MatIndex;
    cv::sortIdx(MatValues, MatIndex, cv::SORT_EVERY_ROW | cv::SORT_DESCENDING);
    return MatIndex;
}

cv::Mat LDN::ProminentDirection(cv::Mat MatIndex, int direction)
{
    std::vector<int> image_size{xx,yy};
    cv::Mat colTop1 = MatIndex.col(direction).clone();
    return colTop1.reshape(1, image_size);
}

void LDN::dump2DInt(cv::Mat m)
{
    std::cout << "[" << endl;
    for (int r(0); r < m.size[0]; ++r) {
        for (int c(0); c < m.size[1]; ++c) {
            std::cout << m.at<int>(r,c) << " ";
        }
        std::cout << ";" << endl;
    }
    std::cout << "]," << endl;
    std::cout << "-----------------------" << endl;
}

void LDN::dump2DFloat(cv::Mat m)
{
    std::cout << "[" << endl;
    for (int r(0); r < m.size[0]; ++r) {
        for (int c(0); c < m.size[1]; ++c) {
            std::cout << m.at<float>(r,c) << " ";
        }
        std::cout << ";" << endl;
    }
    std::cout << "]," << endl;
    std::cout << "-----------------------" << endl;
}

void LDN::dump3DFloat(cv::Mat m)
{
    for (int d(0); d < m.size[2]; ++d) {
        std::cout << "[" << endl;
        for (int r(0); r < m.size[0]; ++r) {
            for (int c(0); c < m.size[1]; ++c) {
                std::cout << m.at<float>(r,c,d) << " ";
            }
            std::cout << ";" << endl;
        }
        std::cout << "]," << endl;
    }
    std::cout << "-----------------------" << endl;
}

void LDN::dump3DInt(cv::Mat m)
{
    for (int d(0); d < m.size[2]; ++d) {
        std::cout << "[" << endl;
        for (int r(0); r < m.size[0]; ++r) {
            for (int c(0); c < m.size[1]; ++c) {
                std::cout << m.at<int>(r,c,d) << " ";
            }
            std::cout << ";" << endl;
        }
        std::cout << "]," << endl;
    }
    std::cout << "-----------------------" << endl;
}
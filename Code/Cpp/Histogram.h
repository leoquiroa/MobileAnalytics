/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Histogram.h
 * Author: mario
 *
 * Created on September 12, 2018, 5:29 PM
 */

#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

class Histogram {
    public:
        cv::Mat calculateHistogram(cv::Mat concatMat);
    private:

};

#endif /* HISTOGRAM_H */


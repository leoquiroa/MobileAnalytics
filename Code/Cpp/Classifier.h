/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Classifier.h
 * Author: mario
 *
 * Created on August 29, 2018, 6:34 PM
 */

#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <opencv2/opencv.hpp>
#include "Tools.h"
#include <vector>                                                                                                                   
#include <numeric>          //avg


using namespace std;

class classifyTable
{
    public:
        int NoFrame;
        float perChanges;
        float bins;
        char type;
    classifyTable(int nf, float pch, float b, char t)
    {
        NoFrame = nf;
        perChanges = pch;
        bins = b;
        type = t;
    }
};

class Classifier 
{
    Tools tool;
    
    public:
        Classifier(int NoFrames, string baseFolder, string folderGray);
        
        vector<classifyTable> classifyVec;
        vector<float> perVec;
        vector<float> binVec;
        
        ofstream LogFile;
        ofstream TableFile;
    private:
        void openFiles(string baseFolder, int NoFrames);
        cv::Mat extractDifferences(string folderGray, int f);
        float getPercentage(cv::Mat result);
        float getNoBins(cv::Mat result);
        void addTransitionFrames(vector<classifyTable> classifyVec);
        void extractColumnsAvg(vector<classifyTable> classifyVec);
        void createTable(vector<classifyTable> classifyVec, float perAvg, float binAvg);
        void closeFiles();
        void clearVectors();
        
        
};

#endif /* CLASSIFIER_H */


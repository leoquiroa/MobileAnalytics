function [s] = cosSim(h1,h2,varargin)
    h2 = repmat(h2,size(h1,1),1);
    prodM = h1.*h2;
    up = sum(prodM,2);
    magh1 = sqrt(sum(((h1).^2),2));
    magh2 = sqrt(sum(((h2).^2),2));
    s = up./(magh1.*magh2);
end
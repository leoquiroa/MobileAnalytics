function [C] = LTP(I,n,threshold)
    %get height and width
    [r,c] = size(I);
    %result of the eight mask convolution
    R = zeros(r,c,8);
    % turn to gray
    if (size(I,3) == 3) 
        I = rgb2gray(I); 
    end
    %convert to double
    I = double(I);
    %mask creation
    M = createMask(n);
    for i=1:8    
       R(:,:,i) = imfilter(I,M(:,:,i),'conv');    
    end
    % discard the magnitude response without sign less than the threshold
    E = abs(R) > threshold;
    % get the positive magnitude response greather than the threshold
    P = (R >= 0).*(E);
    % get the negative magnitude response than the threshold
    N = (R <= 0).*(E);
    % convert to the code
    newC = zeros(r,c,2);
    for i=1:8
       newC(:,:,1) = newC(:,:,1) + P(:,:,i).*2^(i-1);
       newC(:,:,2) = newC(:,:,2) + N(:,:,i).*2^(i-1);
    end
    C = [newC(:,:,1) newC(:,:,2)]+1;
end

function D = createMask(n)
    D = zeros(2*n+1,2*n+1,8);
    % give the center negative value
    D(n+1,n+1,:) = -1; 
    % each of the eight neighbors
    D(n+1,2*n+1,1) = 1;
    D(1,2*n+1,2) = 1;
    D(1,n+1,3) = 1;
    D(1,1,4) = 1;
    D(n+1,1,5) = 1;
    D(2*n+1,1,6) = 1;
    D(2*n+1,n+1,7) = 1;
    D(2*n+1,2*n+1,8) = 1;
end

function [DB] = getFilesByColor(vidHeight,vidWidth,maxFrame,minFrame,FramesPath)
    Ix = 1;
    DB = zeros(vidHeight,vidWidth,(maxFrame-minFrame)+1);       % Original
    for frameNum = minFrame : maxFrame 
        Irgb = imread(strcat(FramesPath,int2str(frameNum),'.png'));
        Ihsv = rgb2hsv(Irgb);
        DB(:,:,Ix) = Ihsv(:,:,1);
        Ix = Ix + 1;
    end
end